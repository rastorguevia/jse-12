package ru.rastorguev.tm.context;

import static ru.rastorguev.tm.view.View.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;

import ru.rastorguev.tm.api.service.*;
import ru.rastorguev.tm.command.*;
import ru.rastorguev.tm.endpoint.*;
import ru.rastorguev.tm.service.*;

import java.lang.Exception;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

@NoArgsConstructor
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final Map<String, AbstractCommand> commandMap = new LinkedHashMap<>();

    @Getter
    @NotNull
    private final ITerminalService terminalService = new TerminalService();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandMap);

    @Getter
    @NotNull
    private final IStateService stateService = new StateService();

    @Getter
    @NotNull
    private final RuRastorguevTmEndpointIDataEndpoint dataEndpoint = new DataEndpointService().getRuRastorguevTmEndpointIDataEndpointPort();

    @Getter
    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();

    @Getter
    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();

    @Getter
    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @Getter
    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    public void init() throws Exception {
        initCommands();
        launchConsole();
    }

    private void initCommands() throws Exception {
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                new Reflections("ru.rastorguev.tm.command").getSubTypesOf(AbstractCommand.class);
        for (@NotNull Class<? extends AbstractCommand> commandClass : classes) {
            @NotNull final AbstractCommand command = commandClass.newInstance();
            command.setServiceLocator(this);
            commandMap.put(command.getName(), command);
        }
    }

    public void launchConsole() throws Exception {
        showWelcomeMsg();

        @Nullable String input = "";
        while (true) {
            try {
                input = terminalService.nextLine().toLowerCase();
                execute(input);
            } catch (Exception e) {
                int exceptionLength = e.getMessage().length();
                printUnderline(exceptionLength);
                System.out.println("\n" + e.getMessage());
                printUnderline(exceptionLength);
                System.out.println();
            }
            System.out.println();
        }
    }

    private void execute(@Nullable final String input) throws Exception {
        if (input == null || input.isEmpty()) {
            showUnknownCommandMsg();
            return;
        }
        @Nullable final AbstractCommand command = commandMap.get(input);
        if (command == null) {
            showUnknownCommandMsg();
            return;
        }
        @NotNull final boolean secureCheck = !command.secure() || (command.secure() && stateService.isAuth());
        @NotNull final boolean roleCheck = command.roles() == null ||
                (command.roles() != null && stateService.isRolesAllowed(command.roles()));
        if (secureCheck && roleCheck) command.execute();
        else showAccessDeniedMsg();
    }

}
