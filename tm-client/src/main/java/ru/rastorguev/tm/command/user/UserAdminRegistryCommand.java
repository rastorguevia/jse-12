package ru.rastorguev.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.endpoint.Role;
import ru.rastorguev.tm.endpoint.Session;
import ru.rastorguev.tm.error.FailException;

@NoArgsConstructor
public final class UserAdminRegistryCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public String getName() {
        return "admin_registry";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Registry new Admin.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Admin registry");

        @Nullable final Session session = serviceLocator.getStateService().getSession();
        if (session == null) throw new FailException("Log in to open new session.");

        System.out.println("Enter login");
        @NotNull final String login = serviceLocator.getTerminalService().nextLine();
        if (login.isEmpty()) throw new FailException("Entered wrong login.");

        System.out.println("Enter password");
        @NotNull final String password = serviceLocator.getTerminalService().nextLine();
        if (password.isEmpty()) throw new FailException("Entered wrong password.");

        serviceLocator.getUserEndpoint().createAdmin(session, login, password);

        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR };
    }

}