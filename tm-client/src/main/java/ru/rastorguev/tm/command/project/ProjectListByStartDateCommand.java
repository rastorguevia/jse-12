package ru.rastorguev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.endpoint.Project;
import ru.rastorguev.tm.endpoint.Role;
import ru.rastorguev.tm.endpoint.Session;
import ru.rastorguev.tm.error.FailException;

import java.util.List;

import static ru.rastorguev.tm.view.View.*;

public class ProjectListByStartDateCommand extends AbstractCommand {
    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public @NotNull String getName() {
        return "project_list_start";
    }

    @Override
    public @NotNull String getDescription() {
        return "Show all projects by start date.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Project list by start date");

        @Nullable final Session session = serviceLocator.getStateService().getSession();
        if (session == null) throw new FailException("Log in to open new session.");

        @NotNull final String sortType = "bystartdate";
        @NotNull final List<Project> projectList =  serviceLocator.getProjectEndpoint().findAllProjectsForUserSorted(session, sortType);
        printAllProjectsForUser(projectList);

        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }

}
