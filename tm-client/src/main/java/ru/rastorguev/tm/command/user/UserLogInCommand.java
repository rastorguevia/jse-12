package ru.rastorguev.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.endpoint.Role;
import ru.rastorguev.tm.endpoint.Session;
import ru.rastorguev.tm.error.FailException;

@NoArgsConstructor
public final class UserLogInCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getName() {
        return "log_in";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "User log in.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Log in");

        System.out.println("Enter login");
        @NotNull final String login = serviceLocator.getTerminalService().nextLine();
        if (login.isEmpty()) throw new FailException("Entered empty login.");

        System.out.println("Enter password");
        @NotNull final String password = serviceLocator.getTerminalService().nextLine();
        if (password.isEmpty()) throw new FailException("Entered empty password.");
        @Nullable final Session session = serviceLocator.getSessionEndpoint().createNewSession(login, password);
        if (session == null) throw new FailException("Cant log in. Try again");

        serviceLocator.getStateService().setSession(session);

        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return null;
    }

}
