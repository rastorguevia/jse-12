package ru.rastorguev.tm.repository;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.repository.IUserRepository;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Role;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

@RequiredArgsConstructor
public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    private final Connection connection;

    @NotNull
    @Override
    public List<User> findAll() throws Exception {
        @NotNull final String sqlQuery = "SELECT * FROM user_table";
        @NotNull final Statement statement = connection.createStatement();
        @NotNull final ResultSet resultSet = statement.executeQuery(sqlQuery);
        @NotNull final List<User> userList = new LinkedList<>();
        while (resultSet.next()) {
            userList.add(resultToUser(resultSet));
        }
        statement.close();
        resultSet.close();
        return userList;
    }

    @Nullable
    @Override
    public User findOne(@NotNull final String userId) throws Exception {
        @NotNull final String sqlQuery = "SELECT * FROM user_table WHERE id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @Nullable User user = null;
        if (resultSet.first()) {
            user = resultToUser(resultSet);
        }
        preparedStatement.close();
        resultSet.close();
        return user;
    }

    @Nullable
    @Override
    public User persist(@NotNull final User user) throws Exception {
        @NotNull final String sqlQuery = "INSERT INTO user_table VALUES(?, ?, ?, ?)";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        preparedStatement.setString(1, user.getId());
        preparedStatement.setString(2, user.getLogin());
        preparedStatement.setString(3, user.getPassHash());
        preparedStatement.setString(4, user.getRole().toString());
        preparedStatement.execute();
        preparedStatement.close();
        return user;
    }

    @Nullable
    @Override
    public User merge(@NotNull final User user) throws Exception {
        @NotNull final String sqlQuery = "UPDATE user_table SET login = ?, passHash = ?, role = ? WHERE id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        preparedStatement.setString(1, user.getLogin());
        preparedStatement.setString(2, user.getPassHash());
        preparedStatement.setString(3, user.getRole().toString());
        preparedStatement.setString(4, user.getId());
        preparedStatement.execute();
        preparedStatement.close();
        return user;
    }

    @Override
    public void remove(@NotNull final String userId) throws Exception {
        @NotNull final String sqlQuery = "DELETE FROM user_table WHERE id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        preparedStatement.setString(1, userId);
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final String sqlQuery = "TRUNCATE user_table";
        @NotNull final Statement statement = connection.createStatement();
        statement.execute(sqlQuery);
        statement.close();
    }

    @Override
    public void userRegistry(@NotNull final String login, @NotNull final String password) throws Exception{
        @NotNull final User user = new User(login, password, Role.USER);
        persist(user);
    }

    @Override
    public void adminRegistry(@NotNull final String login, @NotNull final String password) throws Exception{
        @NotNull final User admin = new User(login, password, Role.ADMINISTRATOR);
        persist(admin);
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) throws Exception {
        @NotNull final String sqlQuery = "SELECT * FROM user_table WHERE login = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        preparedStatement.setString(1, login);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @Nullable User user = null;
        if (resultSet.first()) {
            user = resultToUser(resultSet);
        }
        preparedStatement.close();
        resultSet.close();
        return user;
    }

    @Override
    public boolean isExistByLogin(@NotNull final String login) throws Exception{
        @NotNull final String sqlQuery = "SELECT * FROM user_table WHERE login = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        preparedStatement.setString(1, login);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        return resultSet.first();
    }

    @Override
    public void loadFromDto(@NotNull final List<User> userList) throws Exception {
        for (final User user : userList){
            @Nullable User u = findOne(user.getId());
            if (u == null) persist(user);
            else merge(user);
        }
    }

    @NotNull
    @Override
    public User resultToUser(@NotNull final ResultSet resultSet) throws Exception {
        @NotNull final User user = new User();
        user.setId(resultSet.getString("id"));
        user.setLogin(resultSet.getString("login"));
        user.setPassHash(resultSet.getString("passHash"));
        user.setRole(Role.valueOf(resultSet.getString("role")));
        return user;
    }

}