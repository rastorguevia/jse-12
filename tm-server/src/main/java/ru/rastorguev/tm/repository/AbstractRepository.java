package ru.rastorguev.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.repository.IRepository;
import ru.rastorguev.tm.entity.AbstractEntity;

import java.util.*;

@NoArgsConstructor
public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    @Override
    public abstract List<E> findAll() throws Exception;

    @Nullable
    @Override
    public abstract E findOne(@NotNull final String entityId) throws Exception;

    @Nullable
    @Override
    public abstract E persist(@NotNull final E entity) throws Exception;

    @Nullable
    @Override
    public abstract E merge(@NotNull final E entity) throws Exception;

    @Override
    public abstract void remove(@NotNull final String entityId) throws Exception;

    @Override
    public abstract void removeAll() throws Exception;

}