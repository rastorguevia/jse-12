package ru.rastorguev.tm.repository;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.repository.ISessionRepository;
import ru.rastorguev.tm.entity.Session;
import ru.rastorguev.tm.enumerated.Role;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

@RequiredArgsConstructor
public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @NotNull
    private final Connection connection;

    @NotNull
    @Override
    public List<Session> findAll() throws Exception {
        @NotNull final String sqlQuery = "SELECT * FROM session_table";
        @NotNull final Statement statement = connection.createStatement();
        @NotNull final ResultSet resultSet = statement.executeQuery(sqlQuery);
        @NotNull final List<Session> sessionList = new LinkedList<>();
        while (resultSet.next()) {
            sessionList.add(resultToSession(resultSet));
        }
        statement.close();
        resultSet.close();
        return sessionList;
    }

    @Nullable
    @Override
    public Session findOne(@NotNull final String sessionId) throws Exception {
        @NotNull final String sqlQuery = "SELECT * FROM session_table WHERE id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        preparedStatement.setString(1, sessionId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @Nullable Session session = null;
        if (resultSet.first()) {
            session = resultToSession(resultSet);
        }
        preparedStatement.close();
        resultSet.close();
        return session;
    }

    @Nullable
    @Override
    public Session persist(@NotNull final Session session) throws Exception {
        @NotNull final String sqlQuery = "INSERT INTO session_table VALUES(?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        preparedStatement.setString(1, session.getId());
        preparedStatement.setString(2, session.getUserId());
        if (session.getRole() == null) session.setRole(Role.USER);
        preparedStatement.setString(3, session.getRole().toString());
        preparedStatement.setLong(4, session.getTimestamp());
        preparedStatement.setString(5, session.getSignature());
        preparedStatement.execute();
        preparedStatement.close();
        return session;
    }

    @Nullable
    @Override
    public Session merge(@NotNull final Session session) throws Exception {
        @NotNull final String sqlQuery = "UPDATE session_table SET role = ?, timestamp = ?," +
                " signature = ? WHERE id = ? AND user_id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        if (session.getRole() == null) session.setRole(Role.USER);
        preparedStatement.setString(1, session.getRole().toString());
        preparedStatement.setLong(2, session.getTimestamp());
        preparedStatement.setString(3, session.getSignature());
        preparedStatement.setString(4, session.getId());
        preparedStatement.setString(5, session.getUserId());
        preparedStatement.execute();
        preparedStatement.close();
        return session;
    }

    @Override
    public void remove(@NotNull final String sessionId) throws Exception {
        @NotNull final String sqlQuery = "DELETE FROM session_table WHERE id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        preparedStatement.setString(1, sessionId);
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final String sqlQuery = "TRUNCATE session_table";
        @NotNull final Statement statement = connection.createStatement();
        statement.execute(sqlQuery);
        statement.close();
    }

    @Override
    public boolean isContains(@NotNull final Session session) throws Exception {
        @Nullable final Session checkThisSession = findOne(session.getId());
        if (checkThisSession == null) return false;

        final boolean isUserIdEquals = Objects.equals(checkThisSession.getUserId(), session.getUserId());
        final boolean isCreateDateEquals = Objects.equals(checkThisSession.getTimestamp(), session.getTimestamp());
        final boolean isSignatureEquals = Objects.equals(checkThisSession.getSignature(), session.getSignature());
        final boolean isRoleEquals = Objects.equals(checkThisSession.getRole(), session.getRole());

        return isUserIdEquals && isCreateDateEquals && isSignatureEquals && isRoleEquals;
    }

    @NotNull
    @Override
    public Session resultToSession(@NotNull final ResultSet resultSet) throws Exception {
        @NotNull final Session session = new Session();
        session.setId(resultSet.getString("id"));
        session.setUserId(resultSet.getString("user_id"));
        session.setSignature(resultSet.getString("signature"));
        session.setRole(Role.valueOf(resultSet.getString("role")));
        session.setTimestamp(resultSet.getLong("timestamp"));
        return session;
    }

}
