package ru.rastorguev.tm.repository;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.repository.IProjectRepository;
import ru.rastorguev.tm.entity.Project;
import ru.rastorguev.tm.enumerated.Status;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

@RequiredArgsConstructor
public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @NotNull
    private final Connection connection;

    @NotNull
    @Override
    public List<Project> findAll() throws Exception {
        @NotNull final String sqlQuery = "SELECT * FROM project_table";
        @NotNull final Statement statement = connection.createStatement();
        @NotNull final ResultSet resultSet = statement.executeQuery(sqlQuery);
        @NotNull final List<Project> projectList = new LinkedList<>();
        while (resultSet.next()) {
            projectList.add(resultToProject(resultSet));
        }
        statement.close();
        resultSet.close();
        return projectList;
    }

    @Nullable
    @Override
    public Project findOne(@NotNull final String projectId) throws Exception {
        @NotNull final String sqlQuery = "SELECT * FROM project_table WHERE id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        preparedStatement.setString(1, projectId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @Nullable Project project = null;
        if (resultSet.first()) {
            project = resultToProject(resultSet);
        }
        preparedStatement.close();
        resultSet.close();
        return project;
    }

    @Nullable
    @Override
    public Project persist(@NotNull final Project project) throws Exception {
        @NotNull final String sqlQuery = "INSERT INTO project_table VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        preparedStatement.setString(1, project.getId());
        preparedStatement.setString(2, project.getUserId());
        preparedStatement.setString(3, project.getName());
        preparedStatement.setString(4, project.getDescription());
        preparedStatement.setDate(5, new Date(project.getStartDate().getTime()));
        preparedStatement.setDate(6, new Date(project.getEndDate().getTime()));
        preparedStatement.setString(7, project.getStatus().toString());
        preparedStatement.setLong(8, project.getCreationDate());
        preparedStatement.execute();
        preparedStatement.close();
        return project;
    }

    @Nullable
    @Override
    public Project merge(@NotNull final Project project) throws Exception {
        @NotNull final String sqlQuery = "UPDATE project_table SET name = ?, description = ?," +
                " startDate = ?, endDate = ?, status = ?, creationDate = ? WHERE id = ? AND user_id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        preparedStatement.setString(1, project.getName());
        preparedStatement.setString(2, project.getDescription());
        preparedStatement.setDate(3, new Date(project.getStartDate().getTime()));
        preparedStatement.setDate(4, new Date(project.getEndDate().getTime()));
        preparedStatement.setString(5, project.getStatus().toString());
        preparedStatement.setLong(6, project.getCreationDate());
        preparedStatement.setString(7, project.getId());
        preparedStatement.setString(8, project.getUserId());
        preparedStatement.execute();
        preparedStatement.close();
        return project;
    }

    @Override
    public void remove(@NotNull final String projectId) throws Exception {
        @NotNull final String sqlQuery = "DELETE FROM project_table WHERE id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        preparedStatement.setString(1, projectId);
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final String sqlQuery = "TRUNCATE project_table";
        @NotNull final Statement statement = connection.createStatement();
        statement.execute(sqlQuery);
        statement.close();
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) throws Exception {
        @NotNull final String sqlQuery = "DELETE FROM project_table WHERE user_id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        preparedStatement.setString(1, userId);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @NotNull
    @Override
    public List<Project> findAllByUserId(@NotNull final String userId) throws Exception {
        @NotNull final String sqlQuery = "SELECT * FROM project_table WHERE user_id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Project> projectList = new LinkedList<>();
        while (resultSet.next()) {
            projectList.add(resultToProject(resultSet));
        }
        preparedStatement.close();
        resultSet.close();
        return projectList;
    }

    @NotNull
    @Override
    public List<Project> findProjectsByInputAndUserId(@NotNull final String input, @NotNull final String userId) throws Exception {
        @NotNull final String sqlQuery = "SELECT * FROM project_table WHERE user_id =? AND name LIKE ? OR description LIKE ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, "%" + input.trim() + "%");
        preparedStatement.setString(3, "%" + input.trim() + "%");
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Project> projectList = new LinkedList<>();
        while (resultSet.next()) {
            projectList.add(resultToProject(resultSet));
        }
        preparedStatement.close();
        resultSet.close();
        return projectList;
    }

    @Override
    public void loadFromDto(@NotNull final List<Project> projectList) throws Exception {
        for (final Project project : projectList){
            @Nullable Project p = findOne(project.getId());
            if (p == null) persist(project);
            else merge(project);
        }
    }

    @NotNull
    @Override
    public Project resultToProject(@NotNull final ResultSet resultSet) throws Exception {
        @NotNull final Project project = new Project();
        project.setId(resultSet.getString("id"));
        project.setUserId(resultSet.getString("user_id"));
        project.setName(resultSet.getString("name"));
        project.setDescription(resultSet.getString("description"));
        project.setStartDate(resultSet.getDate("startDate"));
        project.setEndDate(resultSet.getDate("endDate"));
        project.setStatus(Status.valueOf(resultSet.getString("status")));
        project.setCreationDate(resultSet.getLong("creationDate"));
        return project;
    }

}