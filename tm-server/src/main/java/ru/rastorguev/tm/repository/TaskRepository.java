package ru.rastorguev.tm.repository;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.repository.ITaskRepository;
import ru.rastorguev.tm.entity.Task;
import ru.rastorguev.tm.enumerated.Status;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

@RequiredArgsConstructor
public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @NotNull
    private final Connection connection;

    @NotNull
    @Override
    public List<Task> findAll() throws Exception {
        @NotNull final String sqlQuery = "SELECT * FROM task_table";
        @NotNull final Statement statement = connection.createStatement();
        @NotNull final ResultSet resultSet = statement.executeQuery(sqlQuery);
        @NotNull final List<Task> taskList = new LinkedList<>();
        while (resultSet.next()) {
            taskList.add(resultToTask(resultSet));
        }
        statement.close();
        resultSet.close();
        return taskList;
    }

    @Nullable
    @Override
    public Task findOne(@NotNull final String taskId) throws Exception {
        @NotNull final String sqlQuery = "SELECT * FROM task_table WHERE id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        preparedStatement.setString(1, taskId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @Nullable Task task = null;
        if (resultSet.first()) {
            task = resultToTask(resultSet);
        }
        preparedStatement.close();
        resultSet.close();
        return task;
    }

    @Nullable
    @Override
    public Task persist(@NotNull final Task task) throws Exception {
        @NotNull final String sqlQuery = "INSERT INTO task_table VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        preparedStatement.setString(1, task.getId());
        preparedStatement.setString(2, task.getUserId());
        preparedStatement.setString(3, task.getProjectId());
        preparedStatement.setString(4, task.getProjectName());
        preparedStatement.setString(5, task.getName());
        preparedStatement.setString(6, task.getDescription());
        preparedStatement.setDate(7, new Date(task.getStartDate().getTime()));
        preparedStatement.setDate(8, new Date(task.getEndDate().getTime()));
        preparedStatement.setString(9, task.getStatus().toString());
        preparedStatement.setLong(10, task.getCreationDate());
        preparedStatement.execute();
        preparedStatement.close();
        return task;
    }

    @Nullable
    @Override
    public Task merge(@NotNull final Task task) throws Exception {
        @NotNull final String sqlQuery = "UPDATE task_table SET projectName = ?, name = ?, description = ?," +
                " startDate = ?, endDate = ?, status = ?, creationDate = ? WHERE id = ? AND user_id = ? AND project_id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        preparedStatement.setString(1, task.getProjectName());
        preparedStatement.setString(2, task.getName());
        preparedStatement.setString(3, task.getDescription());
        preparedStatement.setDate(4, new Date(task.getStartDate().getTime()));
        preparedStatement.setDate(5, new Date(task.getEndDate().getTime()));
        preparedStatement.setString(6, task.getStatus().toString());
        preparedStatement.setLong(7, task.getCreationDate());
        preparedStatement.setString(8, task.getId());
        preparedStatement.setString(9, task.getUserId());
        preparedStatement.setString(10, task.getProjectId());
        preparedStatement.execute();
        preparedStatement.close();
        return task;
    }

    @Override
    public void remove(@NotNull final String taskId) throws Exception {
        @NotNull final String sqlQuery = "DELETE FROM task_table WHERE id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        preparedStatement.setString(1, taskId);
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final String sqlQuery = "TRUNCATE task_table";
        @NotNull final Statement statement = connection.createStatement();
        statement.execute(sqlQuery);
        statement.close();
    }

    @Override
    public void removeTaskListByProjectId(@NotNull final String projectId) throws Exception {
        @NotNull final String sqlQuery = "DELETE FROM task_table WHERE project_id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        preparedStatement.setString(1, projectId);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    @NotNull
    @Override
    public List<Task> taskListByUserId(@NotNull final String userId) throws Exception {
        @NotNull final String sqlQuery = "SELECT * FROM task_table WHERE user_id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> taskList = new LinkedList<>();
        while (resultSet.next()) {
            taskList.add(resultToTask(resultSet));
        }
        preparedStatement.close();
        resultSet.close();
        return taskList;
    }

    @NotNull
    @Override
    public List<Task> filteredTaskListByUserIdAndInput(@NotNull final String userId, @NotNull final String input) throws Exception{
        @NotNull final String sqlQuery = "SELECT * FROM task_table WHERE user_id =? AND name LIKE ? OR description LIKE ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, "%" + input.trim() + "%");
        preparedStatement.setString(3, "%" + input.trim() + "%");
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> taskList = new LinkedList<>();
        while (resultSet.next()) {
            taskList.add(resultToTask(resultSet));
        }
        preparedStatement.close();
        resultSet.close();
        return taskList;
    }

    @NotNull
    @Override
    public List<Task> taskListByProjectId(@NotNull final String projectId) throws Exception{
        @NotNull final String sqlQuery = "SELECT * FROM task_table WHERE project_id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        preparedStatement.setString(1, projectId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        @NotNull final List<Task> taskList = new LinkedList<>();
        while (resultSet.next()) {
            taskList.add(resultToTask(resultSet));
        }
        preparedStatement.close();
        resultSet.close();
        return taskList;
    }

    @Override
    public void loadFromDto(@NotNull final List<Task> taskList) throws Exception {
        for (final Task task : taskList){
            @Nullable Task t = findOne(task.getId());
            if (t == null) persist(task);
            else merge(task);
        }
    }

    @NotNull
    @Override
    public Task resultToTask(@NotNull final ResultSet resultSet) throws Exception {
        @NotNull final Task task = new Task();
        task.setId(resultSet.getString("id"));
        task.setUserId(resultSet.getString("user_id"));
        task.setProjectId(resultSet.getString("project_id"));
        task.setProjectName(resultSet.getString("projectName"));
        task.setName(resultSet.getString("name"));
        task.setDescription(resultSet.getString("description"));
        task.setStartDate(resultSet.getDate("startDate"));
        task.setEndDate(resultSet.getDate("endDate"));
        task.setStatus(Status.valueOf(resultSet.getString("status")));
        task.setCreationDate(resultSet.getLong("creationDate"));
        return task;
    }

}