package ru.rastorguev.tm.api.service;

import ru.rastorguev.tm.entity.User;

import java.util.List;

public interface IUserService extends IService<User> {

    User findByLogin(final String login) throws Exception;

    boolean isExistByLogin(final String login) throws Exception;

    User createUser(final String login, final String password) throws Exception;

    User createAdmin(final String login, final String password) throws Exception;

    void updateUserLogin(final String userId, final String login) throws Exception;

    void updateUserPassword(final String userId, final String password, final String old) throws Exception;

    void loadFromDto(final List<User> userList) throws Exception;

}
