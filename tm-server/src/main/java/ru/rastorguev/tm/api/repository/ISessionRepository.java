package ru.rastorguev.tm.api.repository;

import ru.rastorguev.tm.entity.Session;

import java.sql.ResultSet;

public interface ISessionRepository extends IRepository<Session> {

    boolean isContains(final Session session) throws Exception;

    Session resultToSession(final ResultSet resultSet) throws Exception;

}
