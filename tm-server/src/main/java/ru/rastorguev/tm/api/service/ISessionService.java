package ru.rastorguev.tm.api.service;

import ru.rastorguev.tm.entity.Session;
import ru.rastorguev.tm.enumerated.Role;

public interface ISessionService extends IService<Session> {

    void validate(final Session session) throws Exception;

    void validateSessionAndRole(final Session session,final Role role) throws Exception;

}
