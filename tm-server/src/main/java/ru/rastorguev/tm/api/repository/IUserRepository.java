package ru.rastorguev.tm.api.repository;

import ru.rastorguev.tm.entity.User;

import java.sql.ResultSet;
import java.util.List;

public interface IUserRepository extends IRepository<User> {

    void userRegistry(final String login, final String password) throws Exception;

    void adminRegistry(final String login, final String password) throws Exception;

    User findByLogin(final String login) throws Exception;

    boolean isExistByLogin(final String login) throws Exception;

    void loadFromDto(final List<User> userList) throws Exception;

    User resultToUser(final ResultSet resultSet) throws Exception;

}
