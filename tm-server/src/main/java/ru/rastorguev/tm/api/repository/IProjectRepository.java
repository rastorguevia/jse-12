package ru.rastorguev.tm.api.repository;

import ru.rastorguev.tm.entity.Project;

import java.sql.ResultSet;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    void removeAllByUserId(final String userId) throws Exception;

    List<Project> findAllByUserId(final String userId) throws Exception;

    List<Project> findProjectsByInputAndUserId(final String input, final String userId) throws Exception;

    void loadFromDto(final List<Project> projectList) throws Exception;

    Project resultToProject(final ResultSet resultSet) throws Exception;

}
