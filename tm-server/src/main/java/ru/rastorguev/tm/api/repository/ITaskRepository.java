package ru.rastorguev.tm.api.repository;

import ru.rastorguev.tm.entity.Task;

import java.sql.ResultSet;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    void removeTaskListByProjectId(final String projectId) throws Exception;

    List<Task> taskListByUserId(final String userId) throws Exception;

    List<Task> filteredTaskListByUserIdAndInput(final String userId, final String input) throws Exception;

    List<Task> taskListByProjectId(final String projectId) throws Exception;

    void loadFromDto(final List<Task> taskList) throws Exception;

    Task resultToTask(final ResultSet resultSet) throws Exception;

}