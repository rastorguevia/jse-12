package ru.rastorguev.tm.api.repository;

import ru.rastorguev.tm.entity.AbstractEntity;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    List<E> findAll() throws Exception;

    E findOne(String entityId) throws Exception;

    E persist(E entity) throws Exception;

    E merge(E entity) throws Exception;

    void remove(String entityId) throws Exception;

    void removeAll() throws Exception;

}
