package ru.rastorguev.tm.error;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class AccessDeniedException extends RuntimeException{

    public AccessDeniedException(String message) {
        super(message);
    }

}
