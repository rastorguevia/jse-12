package ru.rastorguev.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.service.ServiceLocator;
import ru.rastorguev.tm.entity.Project;
import ru.rastorguev.tm.entity.Task;
import ru.rastorguev.tm.entity.User;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@JsonRootName("Dto")
@XmlRootElement(name = "Dto")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataTransferObject implements Serializable {

    private static final long serialVersionUID = 1L;

    @Nullable
    @XmlElement(name = "user")
    @XmlElementWrapper(name = "users")
    @JacksonXmlProperty(localName = "user")
    @JacksonXmlElementWrapper(localName = "users")
    private List<User> users;

    @Nullable
    @XmlElement(name = "project")
    @XmlElementWrapper(name = "projects")
    @JacksonXmlProperty(localName = "project")
    @JacksonXmlElementWrapper(localName = "projects")
    private List<Project> projects;

    @Nullable
    @XmlElement(name = "task")
    @XmlElementWrapper(name = "tasks")
    @JacksonXmlProperty(localName = "task")
    @JacksonXmlElementWrapper(localName = "tasks")
    private List<Task> tasks;

    @Nullable
    @JsonIgnore
    @XmlAnyElement(lax = true)
    public List<Object> anything;

    public void loadDto(@NotNull final ServiceLocator serviceLocator) throws Exception {
        this.users = serviceLocator.getUserService().findAll();
        this.projects = serviceLocator.getProjectService().findAll();
        this.tasks = serviceLocator.getTaskService().findAll();
    }

    public void loadFromDto(@NotNull final ServiceLocator serviceLocator) throws Exception {
        serviceLocator.getUserService().loadFromDto(users);
        serviceLocator.getProjectService().loadFromDto(projects);
        serviceLocator.getTaskService().loadFromDto(tasks);
    }

}
