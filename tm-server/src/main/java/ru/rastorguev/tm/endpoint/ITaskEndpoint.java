package ru.rastorguev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.entity.Session;
import ru.rastorguev.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    @WebMethod
    Task createTask(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "task") final Task task
    ) throws Exception;

    @WebMethod
    Task updateTask(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "task") final Task task
    ) throws Exception;

    @WebMethod
    void removeTask(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "taskId") final String taskId
    ) throws Exception;

    @WebMethod
    void removeTaskListByProjectId(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "projectId") final String projectId
    ) throws Exception;

    @WebMethod
    String taskIdByNumberAndProjectId(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "number") final int number,
            @WebParam(name = "projectId") final String projectId
    ) throws Exception;

    @WebMethod
    List<Task> taskListByUserId(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "userId") final String userId
    ) throws Exception;

    @WebMethod
    List<Task> filteredTaskListByInput(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "input") final String input
    ) throws Exception;

    @WebMethod
    List<Task> getTaskListByProjectId(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "projectId") final String projectId
    ) throws Exception;

    @WebMethod
    List<Task> getTaskListByProjectIdSorted(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectId") @NotNull final String projectId,
            @WebParam(name = "sortType") @NotNull final String sortType
    ) throws Exception;

    @WebMethod
    void removeAllTasks(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception;

    @WebMethod
    Task findTask (
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "taskId") @Nullable final String taskId
    ) throws Exception;

}
