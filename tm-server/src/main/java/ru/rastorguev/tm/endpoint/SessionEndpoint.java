package ru.rastorguev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.service.ServiceLocator;
import ru.rastorguev.tm.entity.Session;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.error.AccessDeniedException;
import ru.rastorguev.tm.util.MD5Util;
import ru.rastorguev.tm.util.SignatureUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.rastorguev.tm.endpoint.ISessionEndpoint")
public final class SessionEndpoint implements ISessionEndpoint {

    private ServiceLocator serviceLocator;

    public SessionEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @Override
    @WebMethod
    public Session createNewSession(
            @WebParam(name = "login") @NotNull final String login,
            @WebParam(name = "password") @NotNull final String password
    ) throws Exception {
        @Nullable final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) throw new AccessDeniedException("This User does not exist.");

        @NotNull final String entryPasswordHash = MD5Util.mdHashCode(password);

        if (!user.getPassHash().equals(entryPasswordHash)) throw new AccessDeniedException("Wrong password.");

        @NotNull Session session = new Session();
        session.setUserId(user.getId());
        session.setRole(user.getRole());
        @Nullable final String signature = SignatureUtil.sign(session);
        if (signature == null || signature.isEmpty()) throw new AccessDeniedException("Something go wrong. Try again.");
        session.setSignature(signature);
        return serviceLocator.getSessionService().persist(session);
    }

    @Override
    @WebMethod
    public void removeSession(@WebParam(name = "session") @NotNull final Session session) throws Exception {
        serviceLocator.getSessionService().remove(session.getId());
    }

}
