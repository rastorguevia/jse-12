package ru.rastorguev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.entity.Session;
import ru.rastorguev.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IUserEndpoint {

    @WebMethod
    User findCurrentUser(@WebParam(name = "session") @Nullable Session session) throws Exception;

    @WebMethod
    User createUser(
            @WebParam(name = "login") @NotNull String login,
            @WebParam(name = "password") @NotNull String password
    ) throws Exception;

    @WebMethod
    User createAdmin(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "login") @NotNull String login,
            @WebParam(name = "password") @NotNull String password
    ) throws Exception;

    @WebMethod
    void updateUserLogin(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "login") @NotNull String login
    ) throws Exception;

    @WebMethod
    void updateUserPassword(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "password") @NotNull String password,
            @WebParam(name = "oldPassword") @NotNull String old
    ) throws Exception;

    @WebMethod
    void removeAllUsers(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception;

    @WebMethod
    boolean isExistByLogin(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login
    ) throws Exception;

}
