package ru.rastorguev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.service.ServiceLocator;
import ru.rastorguev.tm.entity.Session;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Role;
import ru.rastorguev.tm.error.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.rastorguev.tm.endpoint.IUserEndpoint")
public class UserEndpoint implements IUserEndpoint {

    private ServiceLocator serviceLocator;

    public UserEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @Override
    @WebMethod
    public User findCurrentUser(@WebParam(name = "session") @Nullable Session session) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().findOne(session.getUserId());
    }

    @Nullable
    @WebMethod
    public User createUser(
            @WebParam(name = "login") @NotNull String login,
            @WebParam(name = "password") @NotNull String password
    ) throws Exception {
        return serviceLocator.getUserService().createUser(login, password);
    }

    @Nullable
    @WebMethod
    public User createAdmin(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "login") @NotNull String login,
            @WebParam(name = "password") @NotNull String password
    ) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSessionAndRole(session, Role.ADMINISTRATOR);
        return serviceLocator.getUserService().createAdmin(login, password);
    }

    @WebMethod
    public void updateUserLogin(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "login") @NotNull String login
    ) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().updateUserLogin(session.getUserId(), login);
    }

    @WebMethod
    public void updateUserPassword(
            @WebParam(name = "session") @Nullable Session session,
            @WebParam(name = "password") @NotNull String password,
            @WebParam(name = "oldPassword") @NotNull String old
    ) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().updateUserPassword(session.getUserId(), password, old);
    }

    @Override
    @WebMethod
    public void removeAllUsers(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSessionAndRole(session, Role.ADMINISTRATOR);
        serviceLocator.getUserService().removeAll();
    }

    @Override
    @WebMethod
    public boolean isExistByLogin(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "login") @Nullable final String login
    ) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().isExistByLogin(login);
    }

}
