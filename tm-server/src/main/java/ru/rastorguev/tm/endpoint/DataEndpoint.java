package ru.rastorguev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.service.ServiceLocator;
import ru.rastorguev.tm.entity.Session;
import ru.rastorguev.tm.enumerated.Role;
import ru.rastorguev.tm.error.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(name = "ru.rastorguev.tm.endpoint.IDataEndpoint")
public class DataEndpoint implements IDataEndpoint {

    private ServiceLocator serviceLocator;

    public DataEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public void dataSaveBinary(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSessionAndRole(session, Role.ADMINISTRATOR);
        serviceLocator.getDataService().dataSaveBinary();
    }

    @Override
    @WebMethod
    public void dataLoadBinary(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSessionAndRole(session, Role.ADMINISTRATOR);
        serviceLocator.getDataService().dataLoadBinary();
    }

    @Override
    @WebMethod
    public void dataSaveJaxbXml(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSessionAndRole(session, Role.ADMINISTRATOR);
        serviceLocator.getDataService().dataSaveJaxbXml();
    }

    @Override
    @WebMethod
    public void dataLoadJaxbXml(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSessionAndRole(session, Role.ADMINISTRATOR);
        serviceLocator.getDataService().dataLoadJaxbXml();
    }

    @Override
    @WebMethod
    public void dataSaveJaxbJson(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSessionAndRole(session, Role.ADMINISTRATOR);
        serviceLocator.getDataService().dataSaveJaxbJson();
    }

    @Override
    @WebMethod
    public void dataLoadJaxbJson(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSessionAndRole(session, Role.ADMINISTRATOR);
        serviceLocator.getDataService().dataLoadJaxbJson();
    }

    @Override
    @WebMethod
    public void dataSaveFasterXmlXml(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSessionAndRole(session, Role.ADMINISTRATOR);
        serviceLocator.getDataService().dataSaveFasterXmlXml();
    }

    @Override
    @WebMethod
    public void dataLoadFasterXmlXml(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSessionAndRole(session, Role.ADMINISTRATOR);
        serviceLocator.getDataService().dataLoadFasterXmlXml();
    }

    @Override
    @WebMethod
    public void dataSaveFasterXmlJson(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSessionAndRole(session, Role.ADMINISTRATOR);
        serviceLocator.getDataService().dataSaveFasterXmlJson();
    }

    @Override
    @WebMethod
    public void dataLoadFasterXmlJson(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSessionAndRole(session, Role.ADMINISTRATOR);
        serviceLocator.getDataService().dataLoadFasterXmlJson();
    }

}
