package ru.rastorguev.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.entity.Project;
import ru.rastorguev.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @WebMethod
    Project createProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "project") @Nullable final Project project
    ) throws Exception;

    @WebMethod
    Project updateProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "project") @Nullable final Project project
    ) throws Exception;

    @WebMethod
    void removeProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "project") @Nullable final String projectId
    ) throws Exception;

    @WebMethod
    void removeAllProjectsByUser(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception;

    @WebMethod
    List<Project> findAllProjectsForUser(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception;

    @WebMethod
    List<Project> findAllProjectsForUserSorted(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "sortType") @Nullable final String sortType
    ) throws Exception;

    @WebMethod
    List<Project> findProjectsByUserIdAndInput(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "input") @Nullable final String input
    ) throws Exception;

    @WebMethod
    String projectIdByNumber(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "number") final int number
    ) throws Exception;

    @WebMethod
    void removeAllProjects(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception;

    @WebMethod
    Project findProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception;

}
