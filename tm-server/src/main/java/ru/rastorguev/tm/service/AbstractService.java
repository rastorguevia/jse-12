package ru.rastorguev.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.repository.IRepository;
import ru.rastorguev.tm.api.service.IService;
import ru.rastorguev.tm.entity.AbstractEntity;

import java.sql.Connection;
import java.util.List;

import static ru.rastorguev.tm.util.ConnectionUtil.getConnection;

@NoArgsConstructor
public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    public abstract IRepository<E> getRepository(@NotNull final Connection connection);

    @NotNull
    @Override
    public List<E> findAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        @NotNull final List<E> entityList = getRepository(connection).findAll();
        connection.close();
        return entityList;
    }

    @Nullable
    @Override
    public E findOne(@Nullable final String entityId) throws Exception {
        if (entityId == null || entityId.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        @Nullable final E entity = getRepository(connection).findOne(entityId);
        connection.close();
        return entity;
    }

    @Nullable
    @Override
    public E persist(@Nullable final E entity) throws Exception {
        if (entity == null) return null;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try{
            @Nullable final E resultEntity = getRepository(connection).persist(entity);
            connection.commit();
            return resultEntity;
        } catch (Exception e) {
            connection.rollback();
            throw new Exception("Operation failed : " + e);
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public E merge(@Nullable final E entity) throws Exception {
        if (entity == null) return null;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try{
            @Nullable final E resultEntity = getRepository(connection).merge(entity);
            connection.commit();
            return resultEntity;
        } catch (Exception e) {
            connection.rollback();
            throw new Exception("Operation failed : " + e);
        } finally {
            connection.close();
        }
    }

    @Override
    public void remove(@Nullable final String entityId) throws Exception {
        if (entityId == null || entityId.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try{
            getRepository(connection).remove(entityId);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw new Exception("Operation failed : " + e);
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final Connection connection = getConnection();
        getRepository(connection).removeAll();
        connection.close();
    }

}