package ru.rastorguev.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.repository.IRepository;
import ru.rastorguev.tm.api.service.ITaskService;
import ru.rastorguev.tm.entity.Task;
import ru.rastorguev.tm.repository.TaskRepository;

import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import static ru.rastorguev.tm.comparator.EntityComparator.*;
import static ru.rastorguev.tm.comparator.EntityComparator.comparatorStatus;
import static ru.rastorguev.tm.util.ConnectionUtil.getConnection;

@NoArgsConstructor
public final class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    @Override
    public IRepository<Task> getRepository(@NotNull final Connection connection) {
        return new TaskRepository(connection);
    }

    @Override
    public void removeTaskListByProjectId(@Nullable final String projectId) throws Exception {
        if (projectId == null || projectId.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        new TaskRepository(connection).removeTaskListByProjectId(projectId);
        connection.close();
    }

    @Nullable
    @Override
    public String getTaskIdByNumberAndProjectId(final int number, @Nullable String projectId) throws Exception {
        if (projectId == null || projectId.isEmpty()) return null;
        @NotNull List<Task> filteredListOfTasks = taskListByProjectId(projectId);
        if (filteredListOfTasks == null) return null;
        return filteredListOfTasks.get(number - 1).getId();
    }

    @NotNull
    @Override
    public List<Task> taskListByUserId(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final Connection connection = getConnection();
        @NotNull final List<Task> taskList = new TaskRepository(connection).taskListByUserId(userId);
        connection.close();
        return taskList;
    }

    @NotNull
    @Override
    public List<Task> filteredTaskListByUserIdAndInput(@NotNull final String userId, @NotNull final String input) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (input == null || input.isEmpty()) return Collections.emptyList();
        @NotNull final Connection connection = getConnection();
        @NotNull final List<Task> taskList = new TaskRepository(connection).filteredTaskListByUserIdAndInput(userId, input);
        connection.close();
        return taskList;
    }

    @NotNull
    @Override
    public List<Task> taskListByProjectId(@Nullable final String projectId) throws Exception {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @NotNull final Connection connection = getConnection();
        @NotNull final List<Task> taskList = new TaskRepository(connection).taskListByProjectId(projectId);
        connection.close();
        return taskList;
    }

    @NotNull
    @Override
    public List<Task> taskListByProjectIdSorted(@Nullable final String projectId, @Nullable final String sortType) throws Exception {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        if (sortType == null || sortType.isEmpty()) return Collections.emptyList();
        @NotNull final Connection connection = getConnection();
        @NotNull final List<Task> taskList = new TaskRepository(connection).taskListByProjectId(projectId);
        if ("bycreationdate".equals(sortType.toLowerCase())) taskList.sort(comparatorCreationDate);
        if ("byenddate".equals(sortType.toLowerCase())) taskList.sort(comparatorEndDate);
        if ("bystartdate".equals(sortType.toLowerCase())) taskList.sort(comparatorStartDate);
        if ("bystatus".equals(sortType.toLowerCase())) taskList.sort(comparatorStatus);
        connection.close();
        return taskList;
    }

    @Override
    public void loadFromDto(@Nullable final List<Task> taskList) throws Exception {
        if (taskList == null) return;
        @NotNull final Connection connection = getConnection();
        new TaskRepository(connection).loadFromDto(taskList);
        connection.close();
    }

}