package ru.rastorguev.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.repository.IRepository;
import ru.rastorguev.tm.api.repository.ISessionRepository;
import ru.rastorguev.tm.api.service.ISessionService;
import ru.rastorguev.tm.constant.Constant;
import ru.rastorguev.tm.entity.Session;
import ru.rastorguev.tm.enumerated.Role;
import ru.rastorguev.tm.error.AccessDeniedException;
import ru.rastorguev.tm.repository.SessionRepository;

import java.sql.Connection;

import static ru.rastorguev.tm.util.ConnectionUtil.getConnection;

@NoArgsConstructor
public class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    @Override
    public IRepository<Session> getRepository(@NotNull final Connection connection) {
        return new SessionRepository(connection);
    }

    @Override
    public void validate(@Nullable final Session session) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied.");
        @Nullable final String userId = session.getUserId();
        @Nullable final String signature = session.getSignature();
        @Nullable final Role role = session.getRole();
        if (userId == null || userId.isEmpty() || signature == null || signature.isEmpty() || role == null) {
            throw new AccessDeniedException("Access denied.");
        }
        @NotNull final Connection connection = getConnection();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(connection);

        if (!sessionRepository.isContains(session)) {
            connection.close();
            throw new AccessDeniedException("Access denied.");
        }

        final long timeOfExistence = System.currentTimeMillis() - session.getTimestamp();
        if (timeOfExistence > Constant.SESSION_LIFETIME) {
            sessionRepository.remove(session.getId());
            connection.close();
            throw new AccessDeniedException("Session is over.");
        }
        connection.close();
    }

    public void validateSessionAndRole(@Nullable final Session session, @NotNull final Role role) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied.");
        validate(session);
        if (!role.equals(session.getRole())) throw new AccessDeniedException("Access denied. Role type is not allowed.");
    }

}
