package ru.rastorguev.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.repository.IRepository;
import ru.rastorguev.tm.api.service.IProjectService;
import ru.rastorguev.tm.entity.Project;
import ru.rastorguev.tm.repository.ProjectRepository;

import java.sql.Connection;
import java.util.Collections;
import java.util.List;

import static ru.rastorguev.tm.comparator.EntityComparator.*;
import static ru.rastorguev.tm.util.ConnectionUtil.getConnection;

@NoArgsConstructor
public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    @Override
    public IRepository<Project> getRepository(@NotNull final Connection connection) {
        return new ProjectRepository(connection);
    }

    @NotNull
    @Override
    public List<Project> findAllByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final Connection connection = getConnection();
        @NotNull final List<Project> projectList = new ProjectRepository(connection).findAllByUserId(userId);
        connection.close();
        return projectList;
    }

    @NotNull
    @Override
    public List<Project> findAllByUserIdSorted(@Nullable final String userId, @Nullable final String sortType) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (sortType == null || sortType.isEmpty()) return Collections.emptyList();
        @NotNull final Connection connection = getConnection();
        @NotNull final List<Project> projectList = new ProjectRepository(connection).findAllByUserId(userId);
        if ("bycreationdate".equals(sortType.toLowerCase())) projectList.sort(comparatorCreationDate);
        if ("byenddate".equals(sortType.toLowerCase())) projectList.sort(comparatorEndDate);
        if ("bystartdate".equals(sortType.toLowerCase())) projectList.sort(comparatorStartDate);
        if ("bystatus".equals(sortType.toLowerCase())) projectList.sort(comparatorStatus);
        connection.close();
        return projectList;
    }

    @Override
    public void removeAllByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        new ProjectRepository(connection).removeAllByUserId(userId);
        connection.close();
    }

    @Nullable
    @Override
    public String getProjectIdByNumberForUser(final int number, @Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final List<Project> filteredListOfProjects = findAllByUserId(userId);
        return filteredListOfProjects.get(number - 1).getId();
    }

    @NotNull
    @Override
    public List<Project> findProjectsByInputAndUserId(@NotNull final String input, @NotNull final String userId) throws Exception {
        if (userId.isEmpty() || input.isEmpty()) return Collections.emptyList();
        @NotNull final Connection connection = getConnection();
        @NotNull final List<Project> projectList = new ProjectRepository(connection).findProjectsByInputAndUserId(input, userId);
        connection.close();
        return projectList;
    }

    @Override
    public void loadFromDto(@Nullable final List<Project> projectList) throws Exception {
        if (projectList == null) return;
        @NotNull final Connection connection = getConnection();
        new ProjectRepository(connection).loadFromDto(projectList);
        connection.close();
    }

}