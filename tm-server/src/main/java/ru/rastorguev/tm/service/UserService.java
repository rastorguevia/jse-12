package ru.rastorguev.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.repository.IRepository;
import ru.rastorguev.tm.api.service.IUserService;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Role;
import ru.rastorguev.tm.error.AccessDeniedException;
import ru.rastorguev.tm.repository.UserRepository;

import java.sql.Connection;
import java.util.List;

import static ru.rastorguev.tm.util.ConnectionUtil.getConnection;
import static ru.rastorguev.tm.util.MD5Util.mdHashCode;

@NoArgsConstructor
public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    @Override
    public IRepository<User> getRepository(@NotNull final Connection connection) {
        return new UserRepository(connection);
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) return null;
        @NotNull final Connection connection = getConnection();
        @Nullable final User user = new UserRepository(connection).findByLogin(login);
        connection.close();
        return user;
    }

    @Override
    public boolean isExistByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) return false;
        @NotNull final Connection connection = getConnection();
        boolean isExist = new UserRepository(connection).isExistByLogin(login);
        connection.close();
        return isExist;
    }

    @Nullable
    @Override
    public User createUser(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new AccessDeniedException("Wrong login. Try again.");
        if (password == null || password.isEmpty()) throw new AccessDeniedException("Wrong password. Try again.");
        boolean isExist = isExistByLogin(login);
        if (isExist) throw new AccessDeniedException("User already exist.");
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPassHash(mdHashCode(password));
        return persist(user);
    }

    @Nullable
    @Override
    public User createAdmin(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new AccessDeniedException("Wrong login. Try again.");
        if (password == null || password.isEmpty()) throw new AccessDeniedException("Wrong password. Try again.");
        boolean isExist = isExistByLogin(login);
        if (isExist) throw new AccessDeniedException("User already exist.");
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPassHash(mdHashCode(password));
        user.setRole(Role.ADMINISTRATOR);
        return persist(user);
    }

    @Override
    public void updateUserLogin(@Nullable final String userId, @Nullable final String login) throws Exception {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException("User does not exist");
        if (login == null || login.isEmpty()) throw new AccessDeniedException("Login is empty");
        boolean isExist = isExistByLogin(login);
        if (isExist) throw new AccessDeniedException("Login already exist.");
        @Nullable final User user = findOne(userId);
        if (user == null) throw new AccessDeniedException("User does not exist");
        user.setLogin(login);
        merge(user);
    }

    @Override
    public void updateUserPassword(
            @Nullable final String userId,
            @Nullable final String password,
            @Nullable final String old
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException("User does not exist");
        if (password == null || password.isEmpty()) throw new AccessDeniedException("Wrong password. Try again.");
        if (old == null || old.isEmpty()) throw new AccessDeniedException("Wrong old password. Try again.");
        @Nullable final User user = findOne(userId);
        if (user == null) throw new AccessDeniedException("User does not exist");
        final boolean isOldPassEquals = user.getPassHash().equals(mdHashCode(old));
        if (!isOldPassEquals) throw new AccessDeniedException("Old password did not match.");
        user.setPassHash(mdHashCode(password));
        merge(user);
    }

    @Override
    public void loadFromDto(@Nullable final List<User> userList) throws Exception {
        if (userList == null) return;
        @NotNull final Connection connection = getConnection();
        new UserRepository(connection).loadFromDto(userList);
        connection.close();
    }

}