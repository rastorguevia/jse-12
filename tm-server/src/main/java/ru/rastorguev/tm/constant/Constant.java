package ru.rastorguev.tm.constant;

public class Constant {

    public static final String SALT = "saltwhitepoison";

    public static final int CYCLE = 10;

    public static final long SESSION_LIFETIME = 43200000;

    public static final String DATA_ENDPOINT_URL = "http://localhost:8080/DataEndpoint?wsdl";

    public static final String SESSION_ENDPOINT_URL = "http://localhost:8080/SessionEndpoint?wsdl";

    public static final String USER_ENDPOINT_URL = "http://localhost:8080/UserEndpoint?wsdl";

    public static final String PROJECT_ENDPOINT_URL = "http://localhost:8080/ProjectEndpoint?wsdl";

    public static final String TASK_ENDPOINT_URL = "http://localhost:8080/TaskEndpoint?wsdl";

    public static final String DB_PROPERTIES = "db.properties";

    public static final String DB_DRIVER = "db.driver";

    public static final String SERVER_URL = "server.url";

    public static final String DB_URL = "db.url";

    public static final String DB_USER = "db.user";

    public static final String DB_PASSWORD = "db.password";

    public static final String DB_CREATE = "db.create";

    public static final String DB_USE = "db.use";

    public static final String DB_CREATE_USER_TABLE = "db.create.table.user";

    public static final String DB_CREATE_PROJECT_TABLE = "db.create.table.project";

    public static final String DB_CREATE_TASK_TABLE = "db.create.table.task";

    public static final String DB_CREATE_SESSION_TABLE = "db.create.table.session";

}
