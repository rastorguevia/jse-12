package ru.rastorguev.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.enumerated.Role;

import java.util.Date;


@Getter
@Setter
@NoArgsConstructor
public final class Session extends AbstractEntity{

    @Nullable
    private String userId;

    @Nullable
    private Role role;

    private long timestamp = new Date().getTime();

    @Nullable
    private  String signature;

}
