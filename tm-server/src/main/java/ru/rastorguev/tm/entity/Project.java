package ru.rastorguev.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.rastorguev.tm.api.entity.ComparableEntity;
import ru.rastorguev.tm.enumerated.Status;

import java.io.Serializable;
import java.util.Date;

import static ru.rastorguev.tm.util.DateUtil.*;

@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractEntity implements ComparableEntity, Serializable {

    @NotNull
    private String userId;

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Date startDate = stringToDate(dateFormatter.format(new Date()));

    @NotNull
    private Date endDate = stringToDate(dateFormatter.format(new Date()));

    @NotNull
    private Status status = Status.PLANNED;

    @NotNull
    private Long creationDate = System.currentTimeMillis();

}