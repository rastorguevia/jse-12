package ru.rastorguev.tm.context;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.rastorguev.tm.api.service.*;
import ru.rastorguev.tm.constant.Constant;
import ru.rastorguev.tm.endpoint.*;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Role;
import ru.rastorguev.tm.service.*;
import ru.rastorguev.tm.util.PropertyUtil;

import static ru.rastorguev.tm.util.MD5Util.mdHashCode;
import static ru.rastorguev.tm.util.PropertyUtil.*;

import javax.xml.ws.Endpoint;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

@NoArgsConstructor
public final class Bootstrap implements ServiceLocator {

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService();

    @Getter
    @NotNull
    private final IUserService userService = new UserService();

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService();

    @Getter
    @NotNull
    private final IDataService dataService = new DataService(this);

    @NotNull
    private final IDataEndpoint dataEndpoint = new DataEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    public void init() throws Exception {
        prepareDatabase();
        createUsers();
        startServer();
    }

    private void startServer() {
        Endpoint.publish(Constant.USER_ENDPOINT_URL, userEndpoint);
        System.out.println(Constant.USER_ENDPOINT_URL);

        Endpoint.publish(Constant.PROJECT_ENDPOINT_URL, projectEndpoint);
        System.out.println(Constant.PROJECT_ENDPOINT_URL);

        Endpoint.publish(Constant.TASK_ENDPOINT_URL, taskEndpoint);
        System.out.println(Constant.TASK_ENDPOINT_URL);

        Endpoint.publish(Constant.SESSION_ENDPOINT_URL, sessionEndpoint);
        System.out.println(Constant.SESSION_ENDPOINT_URL);

        Endpoint.publish(Constant.DATA_ENDPOINT_URL, dataEndpoint);
        System.out.println(Constant.DATA_ENDPOINT_URL);
    }

    private void prepareDatabase() throws Exception {
        Class.forName(getProperty(Constant.DB_DRIVER));
        @NotNull final String url = PropertyUtil.getProperty(Constant.SERVER_URL);
        @NotNull final String user = PropertyUtil.getProperty(Constant.DB_USER);
        @NotNull final String password = PropertyUtil.getProperty(Constant.DB_PASSWORD);
        @NotNull final Connection connection = DriverManager.getConnection(url, user, password);
        @NotNull final Statement statement = connection.createStatement();
        statement.execute(getProperty(Constant.DB_CREATE));
        statement.execute(getProperty(Constant.DB_USE));
        statement.execute(getProperty(Constant.DB_CREATE_USER_TABLE));
        statement.execute(getProperty(Constant.DB_CREATE_PROJECT_TABLE));
        statement.execute(getProperty(Constant.DB_CREATE_TASK_TABLE));
        statement.execute(getProperty(Constant.DB_CREATE_SESSION_TABLE));
        statement.close();
        connection.close();
        System.out.println("DB - OK");
    }

    private void createUsers() throws Exception {
        try {
            @NotNull final User user = new User();
            user.setId("b4134afc-23d7-4706-8133-3c0fdf63304b");
            user.setLogin("qwerty");
            user.setPassHash(mdHashCode("qwerty"));
            user.setRole(Role.USER);
            userService.persist(user);
            @NotNull final User admin = new User();
            admin.setId("aab8fb8f-8bbc-4480-b561-7140e8353d8a");
            admin.setLogin("admin");
            admin.setPassHash(mdHashCode("qwerty"));
            admin.setRole(Role.ADMINISTRATOR);
            userService.persist(admin);
            System.out.println("DEF USERS - OK");
        }catch (Exception e) {
            System.out.println("Def users already exist.");
        }
    }

}