package ru.rastorguev.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.rastorguev.tm.constant.Constant;

import java.sql.Connection;
import java.sql.DriverManager;

public final class ConnectionUtil {

    @NotNull
    public static Connection getConnection() throws Exception {
        @NotNull final String url = PropertyUtil.getProperty(Constant.DB_URL);
        @NotNull final String user = PropertyUtil.getProperty(Constant.DB_USER);
        @NotNull final String password = PropertyUtil.getProperty(Constant.DB_PASSWORD);
        @NotNull final Connection connection = DriverManager.getConnection(url, user, password);
        if (connection == null) throw new Exception("Connection not created.");
        return connection;
    }

}
