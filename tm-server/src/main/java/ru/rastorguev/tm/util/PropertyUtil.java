package ru.rastorguev.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.constant.Constant;
import ru.rastorguev.tm.error.FileDoesNotExistException;

import java.io.InputStream;
import java.util.Properties;

public final class PropertyUtil {

    @NotNull
    public static String getProperty(@NotNull final String propertyKey) throws Exception{
        @NotNull final Properties properties = new Properties();
        @Nullable final InputStream inputStream = PropertyUtil.class.getClassLoader().getResourceAsStream(Constant.DB_PROPERTIES);
        if (inputStream == null) throw new FileDoesNotExistException("Property file: " + Constant.DB_PROPERTIES + " does not exist.");
        properties.load(inputStream);
        @NotNull final String property = properties.getProperty(propertyKey);
        if (property == null || property.isEmpty()) throw new Exception("Property damaged.");
        inputStream.close();
        return property;
    }

}
