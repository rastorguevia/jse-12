package ru.rastorguev.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.error.AccessDeniedException;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class MD5Util {

    @NotNull
    public static String mdHashCode(@Nullable final String string) throws AccessDeniedException {
        if (string == null || string.isEmpty()) throw new AccessDeniedException("Access Denied. Wrong password.");
        byte[] digest = new byte[0];

        try {
            @NotNull final MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(string.getBytes());
            digest = messageDigest.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        @NotNull final BigInteger bigInt = new BigInteger(1, digest);
        @NotNull final StringBuilder sbHashResult = new StringBuilder(bigInt.toString(16));

        while(sbHashResult.length() < 32){
            sbHashResult.insert(0,"0");
        }

        return sbHashResult.toString();
    }

}