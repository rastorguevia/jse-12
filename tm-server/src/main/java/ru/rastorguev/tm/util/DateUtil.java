package ru.rastorguev.tm.util;

import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateUtil {

    @NotNull
    public final static SimpleDateFormat dateFormatter = new SimpleDateFormat("d.MM.y");

    @NotNull
    public static Date stringToDate(@NotNull final String date) {
        try {
            return dateFormatter.parse(date);
        } catch (ParseException e) {
            return new Date(0);
        }
    }

}