package ru.rastorguev.tm;

import org.jetbrains.annotations.NotNull;
import ru.rastorguev.tm.context.Bootstrap;

public final class Application {

    public static void main(final String[] args) throws Exception {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }

}