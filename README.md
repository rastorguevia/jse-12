https://gitlab.com/rastorguevia/jse-12
# Task Manager
## Software
+ JRE
+ Java 8
+ Maven 3.6.3
+ MySQL Server 5.5
## Developer
Ivan Rastorguev

email: rastorguev.i.a@yandex.ru
## Build App
```
mvn clean install
```
## Run Server-App
```
java -jar tm-server/target/release/bin/tm-server.jar
```
## Run Client-App
```
java -jar tm-client/target/release/bin/tm-client.jar
```
## Open Docs
```
start tm-server/target/release/docs/apidocs/index.html
```